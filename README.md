# `html-do`

**(🚧 Work in progress 🚧)**

HTML in Haskell using `OverloadedLabels` and `RebindableSyntax` (nicer than HTML itself 😛):

```hs
htmlExample = html do
  #head do
    #title "This is a title"
  #body do
    "Hello World!"

    #h1 "Heading level 1"
    #h2 "Heading level 2"
    #h3 "Heading level 3"
    #h4 "Heading level 4"
    #h5 "Heading level 5"
    #h6 "Heading level 6"

    #p "Paragraph"

    #a [#href "https://www.wikipedia.org/"] do
      "A link to Wikipedia!"

    #input [#type "text"    ]
    #input [#type "file"    ]
    #input [#type "checkbox"]

    comment "This is a comment"

    #abbr
      [ #id    "anId"
      , #class "jargon"
      , #style "color:purple;"
      , #title "Hypertext Markup Language"
      ] do
      "HTML"

    #table [#rows "2"] do
      #tr do
        #td [#class "top", #colspan "2", #style "color:red"] do
          #p "Hello, attributes!"
        #td "yay!"
```
