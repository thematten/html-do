{-# options_ghc -Wno-unused-top-binds -Wno-missing-signatures #-}

module HTML.DoSpec (spec) where

import Prelude.HTML.Do

import Control.Monad.IO.Class

import Test.Hspec

spec :: Spec
spec = describe "do syntax" do
  it "works with both 'Monad' and 'HTMLBuilder'" True

-------------------------------------------------------------------------------
htmlExample = html do
  #html do
    #head do
      #title "This is a title"
    #body do
      "Hello World!"

      #h1 "Heading level 1"
      #h2 "Heading level 2"
      #h3 "Heading level 3"
      #h4 "Heading level 4"
      #h5 "Heading level 5"
      #h6 "Heading level 6"

      #p "Paragraph"

      #a [#href "https://www.wikipedia.org/"] do
        "A link to Wikipedia!"

      let input t = html do #input [#type t]

      input "text"
      input "file"
      input "checkbox"

      let blue s = html do #p [#style "color:blue;"] s
      blue "I'm blue"

      comment "This is a comment"

      #abbr
        [ #id    "anId"
        , #class "jargon"
        , #style "color:purple;"
        , #title "Hypertext Markup Language"
        ] do
        "HTML"

      #table [#rows "2"] do
        #tr do
          #td [#class "top", #colspan "2", #style "color:red"] do
            #p "Hello, attributes!"
          #td "yay!"

      raw "<div id=\"raw\" />"

-------------------------------------------------------------------------------
monadExample :: MonadIO m => m ()
monadExample = do
  liftIO $ putStr "What's your name?: "
  name <- liftIO getLine
  liftIO $ putStrLn $ "Hi " ++ name ++ "!"

-------------------------------------------------------------------------------
monadExample1 :: MonadIO m => m a
monadExample1 = do
  liftIO $ putStrLn "Running..."
  monadExample1
