-- | Reexport of "Prelude" with support for "@'html' do@" - you probably want
-- to import this module in place of "Prelude" with @RebindaleSyntax@ enabled,
-- instead of hiding ('Prelude.>>') and importing ('HTML.Do.>>') yourself. See
-- "HTML.Do" on how to use this library.
module Prelude.HTML.Do (module M) where

import Prelude as M hiding ((>>))
import HTML.Do as M
