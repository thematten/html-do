{-# options_ghc -Wno-orphans #-}

{-# language AllowAmbiguousTypes, UndecidableInstances #-}

{-| Combinators for building HTML elegantly using Haskell syntax:

> html do
>   #html do
>     #head do
>       #title "This is a title"
>     #body do
>       "Hello World!"
>
>       #h1 "Heading level 1"
>       #h2 "Heading level 2"
>       #h3 "Heading level 3"
>       #h4 "Heading level 4"
>       #h5 "Heading level 5"
>       #h6 "Heading level 6"
>
>       #p "Paragraph"
>
>       #a [#href "https://www.wikipedia.org/"] do
>         "A link to Wikipedia!"
>
>       #input [#type "text"    ]
>       #input [#type "file"    ]
>       #input [#type "checkbox"]
>
>       comment "This is a comment"
>
>       #abbr
>         [ #id    "anId"
>         , #class "jargon"
>         , #style "color:purple;"
>         , #title "Hypertext Markup Language"
>         ] do
>         "HTML"
>
>       #table [#rows "2"] do
>         #tr do
>           #td [#class "top", #colspan "2", #style "color:red"] do
>             #p "Hello, attributes!"
>           #td "yay!"
>
>       raw "<div id=\"raw\" />"

= How to use it?

Enable @RebindableSyntax@ and import "Prelude.HTML.Do" instead of "Prelude".
Now you can write HTML using "@'html' do@" blocks:

* 'html' block starts like this:

    @
    'html' do
      ..
    @

* plain text is written as a string literal:

    > "Hello world!"

    turns into:

    > Hello world!

* every element/attribute is written using label syntax - e.g. @#table@, @#p@,
  @#href@, ...

* to create empty element, you simply use a label by itself:

    > #br

    turns into:

    > <br />

* to provide content of an element, you simply pass it as an argument to a
  label:

    > #p "Hello world!"

    turns into:

    > <p>Hello world!</p>

* to provide multiple elements as a content, use @do@:

    > #p do
    >   "Hello world!"
    >   #strong "This is cool!"

    turns into:

    > <p>Hello world!\n<strong>This is cool!</strong></p>

* to add attributes to an element, pass them in a list as a first argument:

    > #a [#href "http://haskell.org"]

    turns into:

    > <a href="http://haskell.org" />

    and

    > #a [#href "http://haskell.org"] "Learn more about Haskell!"

    turns into:

    > <a href="http://haskell.org">Learn more about Haskell!</a>

* attributes can be boolean or be assigned some values:

    > #div [#class "functor", #hidden]

    turns into:

    > <div class="functor" hidden />

    and

    > #p [#style "color:red;"] "Hello color!"

    turns into:

    > <p style="color:red;">Hello color!</p>

* HTML comments can be added using 'comment'\/'commentString':

    > comment "I am comment"

    turns into:

    > <!-- I am comment -->

* to embed non-literal 'Text'\/'String', you can use 'text'\/'string':

    > string $ "The answer: " ++ show 42

    turns into:

    > The answer: 42

* you can use @let@ bindings in @do@ blocks:

    > html do
    >   let blue s = html do #p [#style "color:blue;"] s
    >   blue "I'm blue"

    turns into:

    > <p style="color:blue;">I&#39;m blue</p>

* you can embed raw HTML into 'HTML' using 'raw'\/'rawString' (but you have to
  make sure that it is valid and properly escaped yourself!):

    > raw "<p>Hello raw!</p>"

    turns into:

    > <p>Hello raw!</p>

    without any escaping.

To pretty-print 'HTML', use 'prettyHTML'. To render it into some text type, use
'htmlToText'\/'htmlToTextBuilder'\/'htmlToString'.

 -}

module HTML.Do
  ( html
  , comment
  , commentString
  , text
  , string
  , raw
  , rawString
  , prettyHTML
  , htmlToText
  , htmlToTextBuilder
  , htmlToString
  , HTML
  , Attribute
  , (>>)
  -- * Re-exports
  , IsString (fromString)
  , IsLabel  (fromLabel)
  ) where

import           Prelude hiding ((>>))
import qualified Prelude as P

import           Control.Monad hiding   ((>>))
import           Data.Coerce            (coerce)
import           Data.Proxy             (Proxy (Proxy))
import           Data.Text              (Text)
import qualified Data.Text.Lazy as Lazy (Text)
import qualified Data.Text.Lazy.Builder as Lazy
import           GHC.Exts               (IsString (fromString))
import           GHC.OverloadedLabels   (IsLabel, fromLabel)
import           GHC.TypeLits           (KnownSymbol, symbolVal)

import qualified Text.Blaze.Internal        as B
import qualified Text.Blaze.Renderer.Pretty as Pretty
import qualified Text.Blaze.Renderer.String as String
import qualified Text.Blaze.Renderer.Text   as Text

-------------------------------------------------------------------------------
-- | Type of HTML representation. Build one using "@'html' do@"-syntax.
newtype HTML = HTML (B.MarkupM ())
  deriving newtype (Semigroup, Monoid, IsString)

-------------------------------------------------------------------------------
-- | Denotes start of "@'html' do@" block - every separate block describing
-- 'HTML' should start with this keyword. See module description on how to use
-- it.
html :: HTML -> HTML
html = id

{- Note: 'html' function

We ask users to always use 'html' to make sure that top level type of
constructed expression is always known - 'IsLabel'/'IsString' instances
depend on this type to guide type inference and without it being set, users
would get long type error about ambiguity of every single string/label
expression. Plus, it clearly tells reader that upcoming @do@ expression
describes 'HTML', not monadic computation.

 -}

-------------------------------------------------------------------------------
-- | Pretty-prints 'HTML' as 'String', using indentation. Useful for debugging.
prettyHTML :: HTML -> String
prettyHTML = coerce Pretty.renderMarkup

-------------------------------------------------------------------------------
-- | Renders 'HTML' to 'Lazy.Text'.
htmlToText :: HTML -> Lazy.Text
htmlToText = coerce Text.renderMarkup

-------------------------------------------------------------------------------
-- | Renders 'HTML' to 'Lazy.Builder'.
htmlToTextBuilder :: HTML -> Lazy.Builder
htmlToTextBuilder = coerce Text.renderMarkupBuilder

-------------------------------------------------------------------------------
-- | Renders HTML to 'String'.
htmlToString :: HTML -> String
htmlToString = coerce String.renderMarkup

-------------------------------------------------------------------------------
-- | Type of 'HTML' attribute representation. Build one using "@'html' do@"
-- syntax.
newtype Attribute = Attribute { unAttribute :: HTML -> HTML }

-------------------------------------------------------------------------------
-- | Creates 'HTML' comment from 'Text'.
comment :: Text -> HTML
comment = coerce B.textComment

-------------------------------------------------------------------------------
-- | Creates 'HTML' comment from 'String'.
commentString :: String -> HTML
commentString = coerce B.stringComment

-------------------------------------------------------------------------------
-- | Allows one to use 'Text' as plain text 'HTML'. Supplied text will be
-- escaped. Not needed for string literals.
text :: Text -> HTML
text = coerce B.text

-------------------------------------------------------------------------------
-- | Allows one to use 'String' as plain text 'HTML'. Supplied text will be
-- escaped. Not needed for string literals.
string :: String -> HTML
string = coerce B.string

-------------------------------------------------------------------------------
-- | Allows one to use raw 'Text' as 'HTML'. It's up to the user to make sure
-- that supplied HTML is valid and properly escaped.
raw :: Text -> HTML
raw = coerce B.preEscapedText

-------------------------------------------------------------------------------
-- | Allows one to use raw 'String' as 'HTML'. It's up to the user to make sure
-- that supplied HTML is valid and properly escaped.
rawString :: String -> HTML
rawString = coerce B.preEscapedString

-------------------------------------------------------------------------------
-- |
-- > #br
--
-- turns into:
--
-- > <br />
instance KnownSymbol s => IsLabel s HTML where
  fromLabel = leaf $ symbol @s

-------------------------------------------------------------------------------
-- |
-- > #p "Hello world!"
--
-- turns into:
--
-- > <p>Hello world!</p>
instance (KnownSymbol s, builder ~ HTML)
      => IsLabel s (builder -> HTML) where
  fromLabel = parent $ symbol @s

-------------------------------------------------------------------------------
-- |
-- > #a [#href "http://haskell.org"]
--
-- turns into:
--
-- > <a href="http://haskell.org" />
instance {-# incoherent #-}
         (KnownSymbol s, attribute ~ Attribute)
      => IsLabel s ([attribute] -> HTML) where
  fromLabel = foldl @[] (flip unAttribute) $ leaf $ symbol @s

-------------------------------------------------------------------------------
-- |
-- > #a [#href "http://haskell.org"] "Learn more about Haskell!"
--
-- turns into:
--
-- > <a href="http://haskell.org">Learn more about Haskell!</a>
instance (KnownSymbol s, attribute ~ Attribute, builder ~ HTML)
      => IsLabel s ([attribute] -> builder -> HTML) where
  fromLabel attributes builder =
    foldl (flip unAttribute) (parent (symbol @s) builder) attributes

-------------------------------------------------------------------------------
-- |
-- > #div [#hidden]
--
-- turns into:
--
-- > <div hidden />
instance KnownSymbol s => IsLabel s Attribute where
  fromLabel = Attribute $ join attribute $ symbol @s

-------------------------------------------------------------------------------
-- |
-- > #a [#href "http://haskell.org"] "Learn more about Haskell!"
--
-- turns into:
--
-- > <a href="http://haskell.org">Learn more about Haskell!</a>
instance (KnownSymbol s, value ~ Text)
      => IsLabel s (value -> Attribute) where
  fromLabel = Attribute . attribute (symbol @s) . B.Text

-------------------------------------------------------------------------------
class Sequence x y where
  -- | Version of sequencing operator of 'Monad' extended with support for
  -- "@'html' do@" - you want this to be in scope instead of original one, with
  -- @RebindableSyntax@ enabled. See "Prelude.HTML.Do".
  (>>) :: x -> y -> y

instance (Monad m, ma ~ m a) => Sequence ma (m b) where
  (>>) = (P.>>)

instance (builder ~ HTML) => Sequence builder HTML where
  (>>) = (<>)

-------------------------------------------------------------------------------
-- | Reflects 'Symbol' passed as type argument into 'ChoiceString'.
symbol :: forall s. KnownSymbol s => B.ChoiceString
symbol = B.String $ symbolVal $ Proxy @s

-------------------------------------------------------------------------------
-- | Simple empty element with some tag.
leaf :: B.ChoiceString -> HTML
leaf s = HTML $ B.CustomLeaf s True ()

-------------------------------------------------------------------------------
parent :: B.ChoiceString -> HTML -> HTML
parent = coerce $ B.CustomParent @()

-------------------------------------------------------------------------------
attribute :: B.ChoiceString -> B.ChoiceString -> HTML -> HTML
attribute name value = coerce $ B.AddCustomAttribute @() name value
